package userclasses;

import com.mycompany.scanner.MyApplication;
import android.content.Intent;
import com.codename1.impl.android.IntentResultListener;
import userclasses.IntentIntegrator;
import userclasses.IntentResult;
//import com.google.zxing.integration.android.IntentIntegrator;

public class QRCodeGeneratorImpl {
	
	private String qrContents = null;
	private int status;
	public void scanQRCode() {
		status = QRCodeGenerator.PENDING;
		com.codename1.impl.android.CodenameOneActivity ctx = (com.codename1.impl.android.CodenameOneActivity)MyApplication.getContext();
		android.content.Intent intent = new android.content.Intent();
		IntentIntegrator integrator = new IntentIntegrator(ctx);
		integrator.initiateScan();
	    ctx.setIntentResultListener(new IntentResultListener() {
            public void onActivityResult(int requestCode, int resultCode, Intent data) {
            	IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                if (result != null) {
                  String contents = result.getContents();
                  if (contents != null) {
                    status = QRCodeGenerator.OK;
                    qrContents = contents;
                  } else {
                	  System.out.println("Contents Null: ");
                	  status = QRCodeGenerator.ERROR;
                  }
                }
            }
        });
	}
	
	public void getQRCode() {
		status = QRCodeGenerator.PENDING;
		com.codename1.impl.android.CodenameOneActivity ctx = (com.codename1.impl.android.CodenameOneActivity)MyApplication.getContext();
        
		android.content.Intent intent = new android.content.Intent();
		IntentIntegrator integrator = new IntentIntegrator(ctx);
		integrator.shareText("Hello world!!");
	}
	
	public boolean isSupported()
	{
		 return true;
	}
	
	 public int getStatus() {
	        return status;
	    }
	 
	 public String getContents() {
	        return qrContents;
	    }
}
