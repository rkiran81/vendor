package com.mycompany.scanner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import userclasses.QRCodeGenerator;
import userclasses.StateMachine;

import com.codename1.codescan.CodeScanner;
import com.codename1.codescan.ScanResult;
import com.codename1.components.InfiniteProgress;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.io.Util;
import com.codename1.system.NativeLookup;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.events.ActionEvent;
import com.codename1.util.MathUtil;

import dto.UserTransactionSummary;

public class ScannerStateMachine extends StateMachine {
	String resourceFile = null;
	Form mainForm = null;
	Dialog ipDialog;
	
	public ScannerStateMachine(String resFile) {
		super(resFile);
		resourceFile = resFile;
	}
	
//	private Command scannerFormBack = new Command("Back")
//	{
//		public void actionPerformed(ActionEvent evt) {
//			if(mainForm != null)
//				mainForm.showBack();
//			else
//				showMainForm();
//		}
//	};
	
//	@Override
//	protected void postScannerSplash(Form f) {
//		super.postScannerSplash(f);
//		showMainForm();
//	}
	
	@Override
	protected boolean allowBackTo(String formName) {
		// TODO Auto-generated method stub
		System.out.println("allowBackTo: "+formName);
		
		boolean allowBack = true;
		
		if("ScannerSplash".equals(formName))
			allowBack = false;
		return allowBack;
	}	
	
//	private void showMainForm() {
//		mainForm = (Form)createContainer(getResourceFilePath(), "Main");
////		mainForm.addCommand(mainFormBack);
//		mainForm.setBackCommand(mainFormBack);
//		mainForm.show();
//	}
	
	@Override
	protected void beforeMain(Form f) {
		System.out.println("Before main");
		// TODO Auto-generated method stub
		f.addCommand(new Command("Exit") {

			public void actionPerformed(ActionEvent evt) {
				boolean ok = Dialog.show("Exit Vendor App",
						"Do you want to exit?", "OK", "Cancel");
				if (ok)
					exitScanner();
			}

		});
	}
	
	@Override
	protected void onMain_ScanButtonAction(Component c, ActionEvent event) {
		Log.p("Scan Clicked");
//		showForm("ScanResultForm", null);
		CodeScanner.getInstance().scanQRCode(new ScanResult() {
			
			public void scanError(int errorCode, String message) {
				Dialog.show("Scan Error", message, "OK", null);
			}
			
			public void scanCompleted(String contents, String formatName,
					byte[] rawBytes) {
				Log.p("Scan Completed");
				Log.p("contents: "+contents);
				Storage.getInstance().writeObject("SCAN_CONTENTS", contents);
//				showScanResultForm(contents);
				showForm("ScanResultForm", null);
			}
			
			public void scanCanceled() {
				Dialog.show("Scan Cancelled", "Scan Cancelled", "OK", null);
			}
		});
//		if(scanSuccess)
//			showScanResultForm(scacContentBytes);
//		showScanResultForm(null);
	}
	
	@Override
	protected void beforeScanResultForm(Form f) {
		// TODO Auto-generated method stub
		
		String contents = (String)Storage.getInstance().readObject("SCAN_CONTENTS");
//		contents = "{\"endDate\": \"10/15/2013\",\"dealID\": \"2.0\",\"description\": \"Get 15% off\"," +
//				"\"vendorID\": \"1.0\",\"uuid\": \"V5084A 9640e\",\"dealCode\": \"56464456dfg\",\"userID\": \"2.0\"}";
//		contents = "{vendorName=Grand China, dealID=2.0, vendorID=1.0, vendorCity=Banalore, uuid=acbd1234}";
		Log.p("Results Form Scan Contents: "+contents);
		
		JSONParser parser = new JSONParser();
		Log.p("Results Form: "+Display.getInstance().getCurrent().getName());
		
		String startDate = "";
		String endDate = "";
		String description = "";
		String uuid = "";
		
		if(contents != null)
		{
			InputStream input = new ByteArrayInputStream(contents.getBytes());
			Hashtable table = new Hashtable();
			try {
				table = parser.parse(new InputStreamReader(input));
			} catch (IOException e) {
				Dialog.show("Scan Error", e.getMessage(), "OK", null);
				Display.getInstance().exitApplication();
			}
			
			Log.p("Hashtable: "+table.toString());
			
			startDate = (String)table.get("startDate");
			endDate = (String)table.get("endDate");
			description = (String)table.get("description");
			uuid = (String)table.get("uuid");
			
			Storage.getInstance().writeObject("DEAL_CODE", (String)table.get("dealCode"));
			Storage.getInstance().writeObject("DEAL_ID", (String)table.get("dealID"));
			Storage.getInstance().writeObject("VENDOR_ID", (String)table.get("vendorID"));
			Storage.getInstance().writeObject("USER_ID", (String)table.get("userID"));
		}
		
		findExpiresOnTextField(f).setText(endDate);
		findDescriptionTextArea(f).setText(description);
		findUserID(f).setText(uuid);
		
		Log.p("startDate: "+startDate);
		Log.p("endDate: "+endDate);
		Log.p("description: "+description);
	}
	
	private void showScanResultForm(String contents)
	{
		contents = "{\"vendorCity\": \"Banalore\",\"dealID\": \"2.0\",\"vendorName\": \"Grand China\",\"vendorID\": \"1.0\"}";
		JSONParser parser = new JSONParser();
		Log.p("Results Form: "+Display.getInstance().getCurrent().getName());
		
		InputStream input = new ByteArrayInputStream(contents.getBytes());
		Hashtable table = null;
		try {
			table = parser.parse(new InputStreamReader(input));
		} catch (IOException e) {
			Dialog.show("Scan Error", e.getMessage(), "OK", null);
			Display.getInstance().exitApplication();
		}
		
		Log.p("Hashtable: "+table.toString());
		
		String startDate = (String)table.get("startDate");
		String endDate = (String)table.get("endDate");
		String description = (String)table.get("description");
		
		Log.p("startDate: "+startDate);
		Log.p("endDate: "+endDate);
		Log.p("description: "+description);
		
		Form scanResultForm = (Form)createContainer(resourceFile, "ScanResultForm");
//		findFromDate(scanResultForm).setText(startDate);
//		findToDate(scanResultForm).setText(endDate);
//		findDescription(scanResultForm).setText(description);
		
//		scanResultForm.setBackCommand(scannerFormBack);
		scanResultForm.show();
	}
	
	@Override
	protected void onScanResultForm_CancelButtonAction(Component c,
			ActionEvent event) {
		// TODO Auto-generated method stub
		super.onScanResultForm_CancelButtonAction(c, event);
		back();
//		this.back();
	}
	
	@Override
	protected void onScanResultForm_OKButtonAction(final Component c,
			ActionEvent event) {
		// TODO Auto-generated method stub
		super.onScanResultForm_OKButtonAction(c, event);
		String dealCode = (String)Storage.getInstance().readObject("DEAL_CODE");
		String dealID = (String)Storage.getInstance().readObject("DEAL_ID");
		String vendorID = (String)Storage.getInstance().readObject("VENDOR_ID");
		String userID = (String)Storage.getInstance().readObject("USER_ID");
		
		String amount = this.findAmountTextField().getText();
		double points = this.findNumericSpinner().getValue();
		String password = this.findPasswordTextField().getText();
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String todayDate = df.format(new Date());
		
		System.out.println("Amount: "+amount);
		System.out.println("Points: "+points);
		System.out.println("Deal Code: "+dealCode);
		System.out.println("Password: "+password);
		System.out.println("dealID: "+dealID);
		System.out.println("vendorID: "+vendorID);
		System.out.println("userID: "+userID);
		System.out.println("todayDate: "+todayDate);
		
//		UserTransactionSummary uts = new UserTransactionSummary();
//		uts.user.id = Long.parseLong(userID);
//		uts.vendorDeal.id = Long.parseLong(dealID);
//		uts.setTotalpoints(points);
		
		ConnectionRequest r = new ConnectionRequest() {
			Hashtable responseHash = null, resultHash = null;
			@Override
			protected void postResponse() {
				ipDialog.dispose();
				System.out.println("postResponse");
			}

			@Override
			protected void readResponse(InputStream input) throws IOException {
				JSONParser parser = new JSONParser();
				System.out.println("readResponse");
				responseHash = parser.parse(new InputStreamReader(input));
				System.out.println("readResponse Data: "+responseHash);
				Hashtable uts = (Hashtable)responseHash.get("UTS");
				System.out.println("readResponse Data1: "+uts);
				
				System.out.println("Total Points: "+uts.get("totalPoints"));
				System.out.println("updatedAt: "+uts.get("updatedAt"));
				
				Vector userTransactionVector = (Vector)uts.get("userTransaction");
				System.out.println("readResponse Data2: "+userTransactionVector);
				
				for(int i=0; i< userTransactionVector.size(); i++)
				{
					Hashtable data = (Hashtable)userTransactionVector.get(i);
					System.out.println("Amount"+i+1+": "+data.get("amount"));
					System.out.println("points"+i+1+": "+data.get("points"));
					System.out.println("createdAt"+i+1+": "+data.get("createdAt"));
					
					Hashtable dealHash = (Hashtable)data.get("vendorDeal");
					System.out.println("vendorDeal Name"+i+1+": "+dealHash.get("name"));
					System.out.println("vendorDeal Deac"+i+1+": "+dealHash.get("description"));
					
					Hashtable vendor = (Hashtable)dealHash.get("vendor");
					System.out.println("vendor business name"+i+1+": "+vendor.get("vendorbusinessname"));
					
				}
//				if(responseHash != null)
//				{
//					resultHash = (Hashtable)responseHash.get("result");
//					double status = (Double)resultHash.get("code");
//					
//					System.out.println("status: "+status);
//				}
			}

			@Override
			protected void handleException(Exception err) {
				Dialog.show("Yikes!!", "Are you connected to the"
						+ "internet? Check your connection", "OK", null);
				System.out.println(err.getMessage());
				ipDialog.dispose();
			}
		};
		InfiniteProgress ip = new InfiniteProgress();
		ipDialog = ip.showInifiniteBlocking();
		r.setUrl(MyUtil.getURL()+"/"+"ut/create");
//		r.setContentType("application/json");
		r.setPost(true);
		r.addArgument("entity.user.id", userID);
		r.addArgument("entity.points", Double.toString(points));
		r.addArgument("entity.amount", amount);
		r.addArgument("entity.vendorDeal.id", dealID);
		NetworkManager.getInstance().addToQueue(r);
	}
	
	@Override
	protected void onMain_CreateButtonAction(Component c, ActionEvent event) {
		// TODO Auto-generated method stub
		super.onMain_CreateButtonAction(c, event);

				final QRCodeGenerator qrCodeGenerator = (QRCodeGenerator) NativeLookup
						.create(QRCodeGenerator.class);
		        if(qrCodeGenerator != null && qrCodeGenerator.isSupported()) {
		            final Form f = Display.getInstance().getCurrent();
		            System.out.println("zxing is running");
		            new Thread() {
		                public void run() {
		                    System.out.println("zxing invoked");
		                    qrCodeGenerator.getQRCode();
		                    while(qrCodeGenerator.getStatus() == QRCodeGenerator.PENDING) {
		                        try {
		                            sleep(300);
		                        } catch (InterruptedException ex) {
		                            ex.printStackTrace();
		                        }
		                    }
		                    if(qrCodeGenerator.getStatus() == QRCodeGenerator.ERROR) {
		                    	System.out.println("Got error from zxing");
		                    } else {
		                    	String qrContents = qrCodeGenerator.getContents();
		                    	System.out.println("qrContents: " +qrContents);
		                        Dialog.show("QR Code", qrContents, "OK", null);
		                    }
		                    f.animateLayout(800);
		                }
		            }.start();
		        }

	}
	
//	@Override
//	**************** This code scans QR code ********************
//	protected void onMain_CreateButtonAction(Component c, ActionEvent event) {
//		// TODO Auto-generated method stub
//		super.onMain_CreateButtonAction(c, event);
//
//				final QRCodeGenerator qrCodeGenerator = (QRCodeGenerator) NativeLookup
//						.create(QRCodeGenerator.class);
//		        if(qrCodeGenerator != null && qrCodeGenerator.isSupported()) {
//		            final Form f = Display.getInstance().getCurrent();
//		            System.out.println("zxing is running");
//		            new Thread() {
//		                public void run() {
//		                    System.out.println("scan invoked");
//		                    qrCodeGenerator.scanQRCode();
//		                    while(qrCodeGenerator.getStatus() == QRCodeGenerator.PENDING) {
//		                        try {
//		                            sleep(300);
//		                        } catch (InterruptedException ex) {
//		                            ex.printStackTrace();
//		                        }
//		                    }
//		                    if(qrCodeGenerator.getStatus() == QRCodeGenerator.ERROR) {
//		                    	System.out.println("Got error from zxing");
//		                    } else {
//		                    	String qrContents = qrCodeGenerator.getContents();
//		                    	System.out.println("qrContents: " +qrContents);
//		                        Dialog.show("Scan Contents", qrContents, "OK", null);
//		                    }
//		                    f.animateLayout(800);
//		                }
//		            }.start();
//		        }
//
//	}
	
	private void exitScanner()
	{
		Display.getInstance().exitApplication();
	}
}
