/**
 * This class contains generated code from the Codename One Designer, DO NOT MODIFY!
 * This class is designed for subclassing that way the code generator can overwrite it
 * anytime without erasing your changes which should exist in a subclass!
 * For details about this file and how it works please read this blog post:
 * http://codenameone.blogspot.com/2010/10/ui-builder-class-how-to-actually-use.html
*/
package generated;

import com.codename1.ui.*;
import com.codename1.ui.util.*;
import com.codename1.ui.plaf.*;
import com.codename1.ui.events.*;

public abstract class StateMachineBase extends UIBuilder {
    private Container aboutToShowThisContainer;
    /**
     * this method should be used to initialize variables instead of
     * the constructor/class scope to avoid race conditions
     */
    /**
    * @deprecated use the version that accepts a resource as an argument instead
    
**/
    protected void initVars() {}

    protected void initVars(Resources res) {}

    public StateMachineBase(Resources res, String resPath, boolean loadTheme) {
        startApp(res, resPath, loadTheme);
    }

    public Container startApp(Resources res, String resPath, boolean loadTheme) {
        initVars();
        UIBuilder.registerCustomComponent("Button", com.codename1.ui.Button.class);
        UIBuilder.registerCustomComponent("Form", com.codename1.ui.Form.class);
        UIBuilder.registerCustomComponent("InfiniteProgress", com.codename1.components.InfiniteProgress.class);
        UIBuilder.registerCustomComponent("NumericSpinner", com.codename1.ui.spinner.NumericSpinner.class);
        UIBuilder.registerCustomComponent("Label", com.codename1.ui.Label.class);
        UIBuilder.registerCustomComponent("TextArea", com.codename1.ui.TextArea.class);
        UIBuilder.registerCustomComponent("TextField", com.codename1.ui.TextField.class);
        UIBuilder.registerCustomComponent("Container", com.codename1.ui.Container.class);
        if(loadTheme) {
            if(res == null) {
                try {
                    if(resPath.endsWith(".res")) {
                        res = Resources.open(resPath);
                        System.out.println("Warning: you should construct the state machine without the .res extension to allow theme overlays");
                    } else {
                        res = Resources.openLayered(resPath);
                    }
                } catch(java.io.IOException err) { err.printStackTrace(); }
            }
            initTheme(res);
        }
        if(res != null) {
            setResourceFilePath(resPath);
            setResourceFile(res);
            initVars(res);
            return showForm("ScannerSplash", null);
        } else {
            Form f = (Form)createContainer(resPath, "ScannerSplash");
            initVars(fetchResourceFile());
            beforeShow(f);
            f.show();
            postShow(f);
            return f;
        }
    }

    public Container createWidget(Resources res, String resPath, boolean loadTheme) {
        initVars();
        UIBuilder.registerCustomComponent("Button", com.codename1.ui.Button.class);
        UIBuilder.registerCustomComponent("Form", com.codename1.ui.Form.class);
        UIBuilder.registerCustomComponent("InfiniteProgress", com.codename1.components.InfiniteProgress.class);
        UIBuilder.registerCustomComponent("NumericSpinner", com.codename1.ui.spinner.NumericSpinner.class);
        UIBuilder.registerCustomComponent("Label", com.codename1.ui.Label.class);
        UIBuilder.registerCustomComponent("TextArea", com.codename1.ui.TextArea.class);
        UIBuilder.registerCustomComponent("TextField", com.codename1.ui.TextField.class);
        UIBuilder.registerCustomComponent("Container", com.codename1.ui.Container.class);
        if(loadTheme) {
            if(res == null) {
                try {
                    res = Resources.openLayered(resPath);
                } catch(java.io.IOException err) { err.printStackTrace(); }
            }
            initTheme(res);
        }
        return createContainer(resPath, "ScannerSplash");
    }

    protected void initTheme(Resources res) {
            String[] themes = res.getThemeResourceNames();
            if(themes != null && themes.length > 0) {
                UIManager.getInstance().setThemeProps(res.getTheme(themes[0]));
            }
    }

    public StateMachineBase() {
    }

    public StateMachineBase(String resPath) {
        this(null, resPath, true);
    }

    public StateMachineBase(Resources res) {
        this(res, null, true);
    }

    public StateMachineBase(String resPath, boolean loadTheme) {
        this(null, resPath, loadTheme);
    }

    public StateMachineBase(Resources res, boolean loadTheme) {
        this(res, null, loadTheme);
    }

    public com.codename1.ui.Container findContainer4(Component root) {
        return (com.codename1.ui.Container)findByName("Container4", root);
    }

    public com.codename1.ui.Container findContainer4() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container4", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container4", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer3(Component root) {
        return (com.codename1.ui.Container)findByName("Container3", root);
    }

    public com.codename1.ui.Container findContainer3() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container3", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container3", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer2(Component root) {
        return (com.codename1.ui.Container)findByName("Container2", root);
    }

    public com.codename1.ui.Container findContainer2() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container2", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container2", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer1(Component root) {
        return (com.codename1.ui.Container)findByName("Container1", root);
    }

    public com.codename1.ui.Container findContainer1() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container1", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container1", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer5(Component root) {
        return (com.codename1.ui.Container)findByName("Container5", root);
    }

    public com.codename1.ui.Container findContainer5() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container5", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container5", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findUserID(Component root) {
        return (com.codename1.ui.Label)findByName("UserID", root);
    }

    public com.codename1.ui.Label findUserID() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("UserID", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("UserID", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findPointsLabel(Component root) {
        return (com.codename1.ui.Label)findByName("PointsLabel", root);
    }

    public com.codename1.ui.Label findPointsLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("PointsLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("PointsLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findScanResultContainer(Component root) {
        return (com.codename1.ui.Container)findByName("ScanResultContainer", root);
    }

    public com.codename1.ui.Container findScanResultContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("ScanResultContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("ScanResultContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findOKButton(Component root) {
        return (com.codename1.ui.Button)findByName("OKButton", root);
    }

    public com.codename1.ui.Button findOKButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("OKButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("OKButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findPasswordTextField(Component root) {
        return (com.codename1.ui.TextField)findByName("PasswordTextField", root);
    }

    public com.codename1.ui.TextField findPasswordTextField() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("PasswordTextField", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("PasswordTextField", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findUserIDLabel(Component root) {
        return (com.codename1.ui.Label)findByName("UserIDLabel", root);
    }

    public com.codename1.ui.Label findUserIDLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("UserIDLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("UserIDLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer(Component root) {
        return (com.codename1.ui.Container)findByName("Container", root);
    }

    public com.codename1.ui.Container findContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findAmountLabel(Component root) {
        return (com.codename1.ui.Label)findByName("AmountLabel", root);
    }

    public com.codename1.ui.Label findAmountLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("AmountLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("AmountLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextArea findDescriptionTextArea(Component root) {
        return (com.codename1.ui.TextArea)findByName("DescriptionTextArea", root);
    }

    public com.codename1.ui.TextArea findDescriptionTextArea() {
        com.codename1.ui.TextArea cmp = (com.codename1.ui.TextArea)findByName("DescriptionTextArea", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextArea)findByName("DescriptionTextArea", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findAmountTextField(Component root) {
        return (com.codename1.ui.TextField)findByName("AmountTextField", root);
    }

    public com.codename1.ui.TextField findAmountTextField() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("AmountTextField", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("AmountTextField", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findCreateButton(Component root) {
        return (com.codename1.ui.Button)findByName("CreateButton", root);
    }

    public com.codename1.ui.Button findCreateButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("CreateButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("CreateButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.components.InfiniteProgress findInfiniteProgress(Component root) {
        return (com.codename1.components.InfiniteProgress)findByName("InfiniteProgress", root);
    }

    public com.codename1.components.InfiniteProgress findInfiniteProgress() {
        com.codename1.components.InfiniteProgress cmp = (com.codename1.components.InfiniteProgress)findByName("InfiniteProgress", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.components.InfiniteProgress)findByName("InfiniteProgress", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.spinner.NumericSpinner findNumericSpinner(Component root) {
        return (com.codename1.ui.spinner.NumericSpinner)findByName("NumericSpinner", root);
    }

    public com.codename1.ui.spinner.NumericSpinner findNumericSpinner() {
        com.codename1.ui.spinner.NumericSpinner cmp = (com.codename1.ui.spinner.NumericSpinner)findByName("NumericSpinner", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.spinner.NumericSpinner)findByName("NumericSpinner", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findExpiresOnLabel(Component root) {
        return (com.codename1.ui.Label)findByName("ExpiresOnLabel", root);
    }

    public com.codename1.ui.Label findExpiresOnLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("ExpiresOnLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("ExpiresOnLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findDescriptionLabel(Component root) {
        return (com.codename1.ui.Label)findByName("DescriptionLabel", root);
    }

    public com.codename1.ui.Label findDescriptionLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("DescriptionLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("DescriptionLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findPasswordLabel(Component root) {
        return (com.codename1.ui.Label)findByName("PasswordLabel", root);
    }

    public com.codename1.ui.Label findPasswordLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("PasswordLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("PasswordLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findScanButton(Component root) {
        return (com.codename1.ui.Button)findByName("ScanButton", root);
    }

    public com.codename1.ui.Button findScanButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("ScanButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("ScanButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findExpiresOnTextField(Component root) {
        return (com.codename1.ui.TextField)findByName("ExpiresOnTextField", root);
    }

    public com.codename1.ui.TextField findExpiresOnTextField() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("ExpiresOnTextField", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("ExpiresOnTextField", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findScanButtonContainer(Component root) {
        return (com.codename1.ui.Container)findByName("ScanButtonContainer", root);
    }

    public com.codename1.ui.Container findScanButtonContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("ScanButtonContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("ScanButtonContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel(Component root) {
        return (com.codename1.ui.Label)findByName("Label", root);
    }

    public com.codename1.ui.Label findLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findCancelButton(Component root) {
        return (com.codename1.ui.Button)findByName("CancelButton", root);
    }

    public com.codename1.ui.Button findCancelButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("CancelButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("CancelButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    protected void exitForm(Form f) {
        if("ScanResultForm".equals(f.getName())) {
            exitScanResultForm(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(f.getName())) {
            exitMain(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("ScannerSplash".equals(f.getName())) {
            exitScannerSplash(f);
            aboutToShowThisContainer = null;
            return;
        }

    }


    protected void exitScanResultForm(Form f) {
    }


    protected void exitMain(Form f) {
    }


    protected void exitScannerSplash(Form f) {
    }

    protected void beforeShow(Form f) {
    aboutToShowThisContainer = f;
        if("ScanResultForm".equals(f.getName())) {
            beforeScanResultForm(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(f.getName())) {
            beforeMain(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("ScannerSplash".equals(f.getName())) {
            beforeScannerSplash(f);
            aboutToShowThisContainer = null;
            return;
        }

    }


    protected void beforeScanResultForm(Form f) {
    }


    protected void beforeMain(Form f) {
    }


    protected void beforeScannerSplash(Form f) {
    }

    protected void beforeShowContainer(Container c) {
    aboutToShowThisContainer = c;
        if("ScanResultForm".equals(c.getName())) {
            beforeContainerScanResultForm(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(c.getName())) {
            beforeContainerMain(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("ScannerSplash".equals(c.getName())) {
            beforeContainerScannerSplash(c);
            aboutToShowThisContainer = null;
            return;
        }

    }


    protected void beforeContainerScanResultForm(Container c) {
    }


    protected void beforeContainerMain(Container c) {
    }


    protected void beforeContainerScannerSplash(Container c) {
    }

    protected void postShow(Form f) {
        if("ScanResultForm".equals(f.getName())) {
            postScanResultForm(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(f.getName())) {
            postMain(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("ScannerSplash".equals(f.getName())) {
            postScannerSplash(f);
            aboutToShowThisContainer = null;
            return;
        }

    }


    protected void postScanResultForm(Form f) {
    }


    protected void postMain(Form f) {
    }


    protected void postScannerSplash(Form f) {
    }

    protected void postShowContainer(Container c) {
        if("ScanResultForm".equals(c.getName())) {
            postContainerScanResultForm(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(c.getName())) {
            postContainerMain(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("ScannerSplash".equals(c.getName())) {
            postContainerScannerSplash(c);
            aboutToShowThisContainer = null;
            return;
        }

    }


    protected void postContainerScanResultForm(Container c) {
    }


    protected void postContainerMain(Container c) {
    }


    protected void postContainerScannerSplash(Container c) {
    }

    protected void onCreateRoot(String rootName) {
        if("ScanResultForm".equals(rootName)) {
            onCreateScanResultForm();
            aboutToShowThisContainer = null;
            return;
        }

        if("Main".equals(rootName)) {
            onCreateMain();
            aboutToShowThisContainer = null;
            return;
        }

        if("ScannerSplash".equals(rootName)) {
            onCreateScannerSplash();
            aboutToShowThisContainer = null;
            return;
        }

    }


    protected void onCreateScanResultForm() {
    }


    protected void onCreateMain() {
    }


    protected void onCreateScannerSplash() {
    }

    protected void handleComponentAction(Component c, ActionEvent event) {
        Container rootContainerAncestor = getRootAncestor(c);
        if(rootContainerAncestor == null) return;
        String rootContainerName = rootContainerAncestor.getName();
        if(c.getParent().getLeadParent() != null) {
            c = c.getParent().getLeadParent();
        }
        if(rootContainerName == null) return;
        if(rootContainerName.equals("ScanResultForm")) {
            if("OKButton".equals(c.getName())) {
                onScanResultForm_OKButtonAction(c, event);
                return;
            }
            if("CancelButton".equals(c.getName())) {
                onScanResultForm_CancelButtonAction(c, event);
                return;
            }
            if("ExpiresOnTextField".equals(c.getName())) {
                onScanResultForm_ExpiresOnTextFieldAction(c, event);
                return;
            }
            if("DescriptionTextArea".equals(c.getName())) {
                onScanResultForm_DescriptionTextAreaAction(c, event);
                return;
            }
            if("AmountTextField".equals(c.getName())) {
                onScanResultForm_AmountTextFieldAction(c, event);
                return;
            }
            if("PasswordTextField".equals(c.getName())) {
                onScanResultForm_PasswordTextFieldAction(c, event);
                return;
            }
        }
        if(rootContainerName.equals("Main")) {
            if("ScanButton".equals(c.getName())) {
                onMain_ScanButtonAction(c, event);
                return;
            }
            if("CreateButton".equals(c.getName())) {
                onMain_CreateButtonAction(c, event);
                return;
            }
        }
    }

      protected void onScanResultForm_OKButtonAction(Component c, ActionEvent event) {
      }

      protected void onScanResultForm_CancelButtonAction(Component c, ActionEvent event) {
      }

      protected void onScanResultForm_ExpiresOnTextFieldAction(Component c, ActionEvent event) {
      }

      protected void onScanResultForm_DescriptionTextAreaAction(Component c, ActionEvent event) {
      }

      protected void onScanResultForm_AmountTextFieldAction(Component c, ActionEvent event) {
      }

      protected void onScanResultForm_PasswordTextFieldAction(Component c, ActionEvent event) {
      }

      protected void onMain_ScanButtonAction(Component c, ActionEvent event) {
      }

      protected void onMain_CreateButtonAction(Component c, ActionEvent event) {
      }

}
