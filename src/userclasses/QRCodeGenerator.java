package userclasses;



import com.codename1.system.NativeInterface;

public interface QRCodeGenerator extends NativeInterface 
{
	public static final int PENDING = 0;
    public static final int OK = 1;
    public static final int ERROR = 2;
	public void scanQRCode();
	public boolean isSupported();
	public int getStatus();
	public String getContents();
	public void getQRCode();
}
