/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.util.Date;
import java.util.List;

/**
 *
 * @author srikanth
 */
public class User {
    public Long id;

    public String uuid;

    public  String loginId;
    
    public boolean isVendor;

    public String password;
    
    public String facebookToken;

    List<UserTransactionSummary> transactionSummary;
    

    public String firstName;

    public String middleName;

    public String lastName;

    public String emailid;

    public String phoneres;

    public String phoneoff;

    public String address;

    public String city;

    public String area;

    public String state;

    public String country;

    public Integer pincode;
    

    public String sex;

    public Date dob;

    public Date anndate;
   

    public boolean okforsms;

    public Date createdate;

    public Date lastlogin;
  

    public Integer logincount;

    public Integer roleId;

    public String phonemobile;

    public String martialstatus;

    public String annotation;

    public String food;

    public String travel;

    public String sports;

    public String movies;

    public String cosmetics;

    public String automobiles;

    public String jewelery;

    public String music;

    public String health;

    public String books;

    public Date createdAt;

    public Date updatedAt;

    public User() {
    }

    public User(long id) {
        this.id = id;
    }

    

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstname(String firstname) {
        this.firstName = firstname;
    }

    public String getMiddlename() {
        return middleName;
    }

    public void setMiddlename(String middlename) {
        this.middleName = middlename;
    }

    public String getLastname() {
        return lastName;
    }

    public void setLastname(String lastname) {
        this.lastName = lastname;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getPhoneres() {
        return phoneres;
    }

    public void setPhoneres(String phoneres) {
        this.phoneres = phoneres;
    }

    public String getPhoneoff() {
        return phoneoff;
    }

    public void setPhoneoff(String phoneoff) {
        this.phoneoff = phoneoff;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

     

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getAnndate() {
        return anndate;
    }

    public void setAnndate(Date anndate) {
        this.anndate = anndate;
    }

    public boolean getOkforsms() {
        return okforsms;
    }

    public void setOkforsms(boolean okforsms) {
        this.okforsms = okforsms;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Date getLastlogin() {
        return lastlogin;
    }

    public void setLastlogin(Date lastlogin) {
        this.lastlogin = lastlogin;
    }

     

    public Integer getLogincount() {
        return logincount;
    }

    public void setLogincount(Integer logincount) {
        this.logincount = logincount;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getPhonemobile() {
        return phonemobile;
    }

    public void setPhonemobile(String phonemobile) {
        this.phonemobile = phonemobile;
    }

    public String getMartialstatus() {
        return martialstatus;
    }

    public void setMartialstatus(String martialstatus) {
        this.martialstatus = martialstatus;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getTravel() {
        return travel;
    }

    public void setTravel(String travel) {
        this.travel = travel;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getMovies() {
        return movies;
    }

    public void setMovies(String movies) {
        this.movies = movies;
    }

    public String getCosmetics() {
        return cosmetics;
    }

    public void setCosmetics(String cosmetics) {
        this.cosmetics = cosmetics;
    }

    public String getAutomobiles() {
        return automobiles;
    }

    public void setAutomobiles(String automobiles) {
        this.automobiles = automobiles;
    }

    public String getJewelery() {
        return jewelery;
    }

    public void setJewelery(String jewelery) {
        this.jewelery = jewelery;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "models.User[ id=" + id + " ]";
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
   
}
