/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.util.Date;

/**
 *
 * @author srikanth
 */
public class UserTransactionSummary {
    private Long id;
     

    public User user;
     

     public VendorDeal vendorDeal;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation

    private double totalPoints;


    private Date date;

    public Date createdAt;

    private Date updatedAt;

    public UserTransactionSummary() {
    }

    

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public VendorDeal getVendorDeal() {
        return vendorDeal;
    }

    public void setVendorDeal(VendorDeal vendorDeal) {
        this.vendorDeal = vendorDeal;
    }

    public double getTotalPoints() {
        return totalPoints;
    }

    public void setTotalpoints(double totalpoints) {
        this.totalPoints = totalpoints;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    
    
    public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}

    @Override
    public String toString() {
        return "models.UserTransactionSummaries[ id=" + id + " ]";
    }
    
}
