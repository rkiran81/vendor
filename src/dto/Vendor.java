/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.util.Date;

/**
 *
 * @author srikanth
 */
public class Vendor {

    public Long id;

    public String uuid;

    public Integer privateKey;

    public Integer publicKey;

    public String vendorbusinessname;

    public String vendorfirstname;

    public String vendormiddlename;

    public String vendorlastname;

    public String vendorbusinessaddress;

    public String vendorCity;

    public String vendorCityArea;

    public String vendortelephonenumber;

    public String vendorbusinesslocation;

    public String vendorpassword;

    public String vendormailid;

    public String vendorsubid;

    public Date createdAt;

    public Date updatedAt;

    public  User owner;

    public Vendor() {
    }

    public Vendor(long id) {
        this.id = id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(Integer privateKey) {
        this.privateKey = privateKey;
    }

    public Integer getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(Integer publicKey) {
        this.publicKey = publicKey;
    }

    public String getVendorbusinessname() {
        return vendorbusinessname;
    }

    public void setVendorbusinessname(String vendorbusinessname) {
        this.vendorbusinessname = vendorbusinessname;
    }

    public String getVendorfirstname() {
        return vendorfirstname;
    }

    public void setVendorfirstname(String vendorfirstname) {
        this.vendorfirstname = vendorfirstname;
    }

    public String getVendormiddlename() {
        return vendormiddlename;
    }

    public void setVendormiddlename(String vendormiddlename) {
        this.vendormiddlename = vendormiddlename;
    }

    public String getVendorlastname() {
        return vendorlastname;
    }

    public void setVendorlastname(String vendorlastname) {
        this.vendorlastname = vendorlastname;
    }

    public String getVendorbusinessaddress() {
        return vendorbusinessaddress;
    }

    public void setVendorbusinessaddress(String vendorbusinessaddress) {
        this.vendorbusinessaddress = vendorbusinessaddress;
    }

    public String getVendortelephonenumber() {
        return vendortelephonenumber;
    }

    public void setVendortelephonenumber(String vendortelephonenumber) {
        this.vendortelephonenumber = vendortelephonenumber;
    }

    public String getVendorbusinesslocation() {
        return vendorbusinesslocation;
    }

    public void setVendorbusinesslocation(String vendorbusinesslocation) {
        this.vendorbusinesslocation = vendorbusinesslocation;
    }

    public String getVendorpassword() {
        return vendorpassword;
    }

    public void setVendorpassword(String vendorpassword) {
        this.vendorpassword = vendorpassword;
    }

    public String getVendormailid() {
        return vendormailid;
    }

    public void setVendormailid(String vendormailid) {
        this.vendormailid = vendormailid;
    }

    public String getVendorsubid() {
        return vendorsubid;
    }

    public void setVendorsubid(String vendorsubid) {
        this.vendorsubid = vendorsubid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "models.Vendors[ id=" + id + " ]";
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
