/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;


/**
 *
 * @author srikanth
 */
public class VendorDeal {

    public Long id;

    private Vendor vendor;

    public String dealCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getDealCode() {
		return dealCode;
	}

	public void setDealCode(String dealCode) {
		this.dealCode = dealCode;
	}

    
}
